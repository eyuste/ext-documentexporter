/**
 *
 */
package com.aspaconsultores.documentexporter.exporter;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.media.MediaModel;

import com.aspaconsultores.aspabills.data.BillData;


/**
 * @author David Mar� Suarez based on Carlos�s feeds Service
 *
 */
public interface BillsExporter
{

	/**
	 * @param billData
	 * @param site
	 */
	MediaModel exportBill(BillData billdata, CMSSiteModel site, String code);



	/**
	 * @param media
	 * @param site
	 * @param code
	 */
	MediaModel createPDF(MediaModel media, CMSSiteModel site, String code, String reportCode);
}
