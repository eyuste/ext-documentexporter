/**
 * 
 */
package com.aspaconsultores.aspabills.writer;

import java.io.Writer;

import com.aspaconsultores.aspabills.data.BillData;


/**
 * @author David Mari Suarez based on Carlos feeds Service
 * 
 */
public interface BillWriter
{
	String getMimeType();

	String getFileExtension();

	void writeBillStream(BillData content, Writer out);
}
