/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.aspaconsulting.documentexporter.constants;

/**
 * Global class for all Aspadocumentexporter constants. You can add global constants for your extension into this class.
 */
public final class AspadocumentexporterConstants extends GeneratedAspadocumentexporterConstants
{
	public static final String EXTENSIONNAME = "aspadocumentexporter";

	private AspadocumentexporterConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

    public static final String PLATFORM_LOGO_CODE = "aspadocumentexporterPlatformLogo";
}
