/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.aspaconsulting.documentexporter.strategies;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.core.model.media.MediaModel;

import com.aspaconsulting.documentexporter.exceptions.UnnsuportedDataException;


/**
 *
 * #author Ezequiel Yuste
 */
public interface DocumentExporterStrategy
{

	/**
	 * This method receive a data object and store the data object into a XML document
	 *
	 * @param data
	 *           the object datta to fill the pdf template
	 * @param site
	 *           site of the catalog where will be stored the xml document
	 * @param code
	 *           the code of the DocumentModel that will be stored as xml document
	 *
	 * @throws UnnsuportedDataException
	 *            When received data is unnsuported by the method
	 * @result the xml media model generated
	 */
	MediaModel exportObjectData(Object data, CMSSiteModel site, String code) throws UnnsuportedDataException;

	/**
	 * This method receive a media model to fill the pdf document and return another media with the pdf content
	 *
	 * @param media
	 *           a media model which contains information to fill the pdf document (could be null for itext strategy)
	 * @param site
	 *           site of the catalog where will be stored the pdf document
	 * @param code
	 *           the code of the pdf document model that will be stored
	 * @param reportCode
	 *           the code of the media which contains the pdf template
	 *
	 * @return the media model with the pdf generated
	 *
	 */
	MediaModel createPDF(MediaModel media, CMSSiteModel site, String code, String reportCode);

}
