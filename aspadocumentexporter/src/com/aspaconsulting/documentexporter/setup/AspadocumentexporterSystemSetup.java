/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.aspaconsulting.documentexporter.setup;

import static com.aspaconsulting.documentexporter.constants.AspadocumentexporterConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import com.aspaconsulting.documentexporter.constants.AspadocumentexporterConstants;
import com.aspaconsulting.documentexporter.service.AspadocumentexporterService;


@SystemSetup(extension = AspadocumentexporterConstants.EXTENSIONNAME)
public class AspadocumentexporterSystemSetup
{
	private final AspadocumentexporterService aspadocumentexporterService;

	public AspadocumentexporterSystemSetup(final AspadocumentexporterService aspadocumentexporterService)
	{
		this.aspadocumentexporterService = aspadocumentexporterService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		aspadocumentexporterService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return AspadocumentexporterSystemSetup.class.getResourceAsStream("/aspadocumentexporter/sap-hybris-platform.png");
	}
}
