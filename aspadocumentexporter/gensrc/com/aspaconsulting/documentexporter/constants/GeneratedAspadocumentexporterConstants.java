/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 27-mar-2017 16:34:43                        ---
 * ----------------------------------------------------------------
 */
package com.aspaconsulting.documentexporter.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedAspadocumentexporterConstants
{
	public static final String EXTENSIONNAME = "aspadocumentexporter";
	
	protected GeneratedAspadocumentexporterConstants()
	{
		// private constructor
	}
	
	
}
